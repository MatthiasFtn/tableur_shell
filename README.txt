MAIO Antony
FONTAINE Matthias


Pour executer:
./tableur -in fichier_src -out fichier_dest -scin sep_in -scout sep_out -slin sep_lin -slout sep_lout -inverse


Fonctionalités réalisées: *ligne dans le fichier indiqué*

Ligne 10 – [cel] : permet d’obtenir la valeur de la cellule cel ;
Ligne 22 – +(val1,val2) : somme de val1 et val2 où vali est soit un valeur numérique;
Ligne 31 – -(val1,val2) : différence entre val1 et val2 ;
Ligne 38 – *(val1,val2) : produit de val1 par val2 ;
Ligne 45 – /(val1,val2) : quotient de val1 et val2 ;
Ligne 52 – ˆ(val1,val2) : élève val1 à la puissance val2 ;
Ligne 60 – ln(val1) : calcule le logarithme népérien de val1 ;
Ligne 67 – e(val1) : calcule l’exponentiation de e à la puissance val1 ;
Ligne 75 – sqrt(val1) : calcule la racine carrée de val1 ;
Ligne 83 – somme(cel1,cel2) : calcule la somme des cellules appartenant à l’intervalle de cel1 à cel2 (ex : =somme(l1c1,l2c4) effectue la somme des cellules l1c1, l1c2, l1c3, l1c4, l2c1, l2c2, l2c3, l2c4 ;
Ligne 117 – moyenne(cel1,cel2) : calcule la moyenne des cellules appartenant à l’intervalle de cel1 à cel2 ;
Ligne 124 – min(cel1,cel2) : fournit la plus petite valeur des cellules de l’intervalle de cel1 à cel2 ;
Ligne 154 – max(cel1,cel2) : fournit la plus grande valeur des cellules de l’intervalle de cel1 à cel2 ;
Ligne 188 – concate(val) : fournit la longueur de la chaîne de caractères val ;
Ligne 195 – length(val) : fournit la longueur de la chaîne de caractères val ;
Ligne 204 – subsitute(val1,val2,val3) : remplace dans la chaîne de caractères val1, la chaîne val2 par val3 ;
Ligne 220 – size(val) : récupère la taille du fichier identifié par val ;
Ligne 235 – lines(val) : récupère le nombre de lignes du fichier identifié par val ;


Fonctionalités non réalisées: 

– - inverse : cette option provoque l’inversion lignes/colonnes de la feuille calculée
– shell(val) : remplace le contenu de la cellule par le résultat de la commande val ;
– display(cel1,cel2) : permet de spécifier un ensemble de cellules à afficher. Si cette commande apparaît
  dans la feuille de calculs, seul l’ensemble de cellules défini par cel1 et cel2 sera fourni en sortie. Si cette
  commande apparaît plusieurs fois, tous les ensembles définis seront fournis en sortie. Si cette commande
  n’apparaît pas dans la feuille de calculs (comportement par défaut) toute la feuille de calculs est considérée
  en sortie.
– +(val1,val2) : soit une référence à une cellule, soit le résultat d’un autre calcul (ex : =+(3,+(l2c4,7)) effectue la somme de 3 avec le résultat de la somme de 7 et du contenu de la cellule de ligne numéro 2 et de colonne 4) ;




Difficultés:

Durant le projet nous avons eu beaucoup de difficultés, comme celles notés ci-dessous par exemple, ou même d'autres dont nous ne nous en souvenons pas spécialement au moment où nous
rédigeon ces quelques lignes.

- Ajouter les retours à la ligne après avoir entrer dans la sortie standard (terminal) la fiche de calcul ce qui fait que toute est écrit à la suite et écrase la première valeur de la ligne suivante.
- Après lecture du fichier et écriture dans un autre fichier, toute est écrit à la suite, ne passe pas à la ligne et affiche '\n'.
- Creation de fichiers temporaires à partir du fichier source pour créer un fichier de destination. Il fallait ensuite supprimer ces fichiers temporaire créer.
- Récupérer la ligne et la colonne ciblé.
- Les séparateurs : que ce soit sur la lecture d'une ligne, la lecture de paramètre dans une fonction, ou tout simplement séparateur entrée/sortie dans le fichier.
- Lecture et ecriture dans un fichier.
- Lire les fonctions dans le fichier et les executer.
- Faire les fonctions un peu plus complexes (vareiances, ecart type, mediannes, shell, display) qui non pas étaient implémentés au final.
- Faire l'inversion des lignes et des colonnes qui n'a pas étaient implementé.
- Addition, soustraction, division, produit, etc ...   sur une référence à une cellule, soit le résultat d’un autre calcul n'a pas était implémentés.
 
Nous avons aussi conscience que nous manquont de beaucoup de pratique et de connaissance, ce qui nous à beaucoup mis en difficultés.



Part de travail:
MAIO Antony - 50%
FONTAINE Matthias - 50%
